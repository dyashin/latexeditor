/**
 * Created by Ikar on 31.07.2015.
 */
(function($){
    "use strict";
    var previewDomNode,
        QUEUE = MathJax.Hub.queue,
        math = null,
        deferred,
        modalNode,
        inputNode;

    $.fn.latexModal = function(options){
        deferred = $.Deferred();
        modalNode = this.modal(options);
        initModalWindow();
        return deferred.promise();
    };



    QUEUE.Push(function () {
        math = MathJax.Hub.getAllJax(previewDomNode)[0];
    });

    function initModalWindow(){
        checkPreviewDomNode(modalNode);
        modalNode.on('shown.bs.modal', function(){
            inputNode = modalNode.find('.latex-input')
                .on('keyup', function(){
                    updateMath(this.value);
                });
        });

        $('.close, .cancel-button').on('click', function(){
            modalNode.modal('hide');
            deferred.reject('action canceled');
        });

        $('.success-button').on('click', function(){
            modalNode.modal('hide');
            deferred.resolve(inputNode.val());
        });

    }

    function updateMath (TeXString) {
        QUEUE.Push(["Text", math, "\\displaystyle{" + TeXString + "}"]);
    }

    function checkPreviewDomNode(modal){
        if(!previewDomNode){
            previewDomNode = modal.find('.formula-preview')[0];
        }
    }

}(jQuery));
