/**
 * Created by Ikar on 31.07.2015.
 */
$(function(){
    $('.show-latex-modal').on('click', function(){
        $('#exampleModal').latexModal()
            .done(function(latexString){
                console.log(latexString);
            })
            .fail(function(message){
                console.log(message);
            });
    });
});
